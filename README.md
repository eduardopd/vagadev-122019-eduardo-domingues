# Avaliação Front-End N1 #

Leia atentamente até o final.

Nossa avaliação é bem simples, pois o intuito é analisar como o candidato desenvolve os itens solicitados.
Avaliamos qualidade de código, versionamento, uso de automatizadores, uso de preprocessadores e javascript(vanilla, jquery ou react).


*OBS.: Evite bootstrap e outros similares, pois queremos avaliar o seu código na implementação dos itens.*

### O Básico a ser executado para concorrer a vaga ###
* Fork o repositório e inicie o desenvolvimento;
* Desenvolva o layout que está no arquivo .xd. O projeto deve ser responsivo. Lembrando que no layout só existe a versão desktop. Sendo assim adapte a responsividade como preferir.

Finalizando esses itens você terá terminado o level 01 da avaliação.

*OBS.: Interações e funcionalidades não sugeridas no layout serão levadas em consideração.*

### Ultrapassando a concorrência ###
* Observe que no layout existe um modal(Quick cart) de sucesso da compra. Ele deve aparecer quando clicar no botão de comprar.
* Prever também a interação com o quick cart quando o usuário clicar no comprar do compre junto.

* **PLUS:** Junto com a ação de exibição do modal atualize a quantidade de itens no carrinho presente no header.

Finalizando esses itens você terá terminado o level 02 da avaliação.

### CHEFÃO para garantir a vaga: ###
* Desenvolver um autocomplete para busca no header.

*OBS. 01: O candidato esta livre para trabalhar com a estrutura e tecnologia que preferir, exceto bootstrap e similares.*

*OBS. 02: Qualquer dúvida você pode entrar em contato comigo pelo e-mail: rafael.augusto@agencian1.com.br.*
